#!/bin/bash

git pull --rebase --no-edit git@github.com:beagleboard/BeagleBoard-DeviceTrees.git v5.10.x-ti-unified
git pull --rebase --no-edit git@git.beagleboard.org:beagleboard/BeagleBoard-DeviceTrees.git v5.10.x-ti-unified
